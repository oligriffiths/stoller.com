<?php
/**
 * The base configurations of the WordPress.
 *
 * This file has the following configurations: MySQL settings, Table Prefix,
 * Secret Keys, WordPress Language, and ABSPATH. You can find more information
 * by visiting {@link http://codex.wordpress.org/Editing_wp-config.php Editing
 * wp-config.php} Codex page. You can get the MySQL settings from your web host.
 *
 * This file is used by the wp-config.php creation script during the
 * installation. You don't have to use the web site, you can just copy this file
 * to "wp-config.php" and fill in the values.
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'stoller');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', 'root');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY', 'N$T+cWymW&{]k{$-awl/lzXRp*wjhbvdMFQiQhRlzjvK%x>OERfibw?Cw*PK!YQzf?(c{N>UXG$<syN_LCrl<dVmJ)DFoo>F_SiG/[aj!kpNBQw]Bp;$TbjUh[SJ@OaS');
define('SECURE_AUTH_KEY', '=k><pL}jA?<kHNJIt+dmryHvZ$f[<}>}VMuYvBRo{HSZT$CR<F/L*Dn;N]CIZnMqag]@&vEa{NP?s&Sv&sBFLwrgY!$TdgXDfo_&!LkVIsq!cgot](VZ+tI@sDZ>luup');
define('LOGGED_IN_KEY', '>^YJ(|{aEFyaB)FXLSQmMqp-=t=$@Dk&e!?hJxIym_Ag]@sSMMX>Fap}+DY;K+VSA&I_Zt(EHg?(g!MX]]X!g[WuSU>(|Vy*=e{XeaUEL-)C%^PNBS*KTU^P/NcWcbyc');
define('NONCE_KEY', 'UZY&}fFk?]{*@JH_xSVW-hJ-IKS++t|ZNZ|Si!RoZNJ_DLM?L=(Ef^xFBFqCJ-rFeIj+)c*ZVjr/}o;i{;Wj$%LwjjvbChCpX+[}I?^Uhn?UWrpE{H]yD<JcQHd)LVr!');
define('AUTH_SALT', 'yiaDIH-MGSY*yS*p}ZZgSjN!+/$D{)gQQRd)$sKVtnb%@T_mV>;PPLf]AXLx{^C/XlWi]>>@Zs=mODr?r/mNhOY/)]a@(%RlD&bm?P+J>(xINdzpo{PiJFkb({n!FJnk');
define('SECURE_AUTH_SALT', 'Yq_B{^+s%r(C{Uj>|?uGfbP=cvScH%aND}t%RRtuLZrl!lZzifFb)aQL{DAh-aW<iCS+DS%QiHpHpSKl^<@iBNS?*=Og[Py@huKAitc!wUsfH?SVOT@Kt;WFJmM_CbBI');
define('LOGGED_IN_SALT', 'DDxePA;A*!nVv>_;EN@&+xuql$f]^S-x?L??<*U[B)TK{RIyRqQE+QcvNdn+^b]<w{aclnR>F<|UxWVLXF{v?=j]W&^K/XUBEr%h>^hFACbt+xAiEIrIKSpI}neZJxrj');
define('NONCE_SALT', 'n=)@{jClFMP)]-bhvRf(A!}JqOde@ZqBF?<OtKe<Q+EgAmq(a)W@By%eJ=]xxTin&uGpEzmZs)P{=EHgM%{$V=GboWLePC}X+G|S%]eXw&Up;_>@?GHeY}It]t)PkWOY');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each a unique
 * prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_weep_';

/**
 * WordPress Localized Language, defaults to English.
 *
 * Change this to localize WordPress. A corresponding MO file for the chosen
 * language must be installed to wp-content/languages. For example, install
 * de_DE.mo to wp-content/languages and set WPLANG to 'de_DE' to enable German
 * language support.
 */
define('WPLANG', '');

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
