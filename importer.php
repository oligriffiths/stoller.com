<?php
/**

#Add the terms

INSERT IGNORE INTO wp_weep_terms (SELECT 0, region, LOWER(region), 0, 0 FROM products);

INSERT IGNORE INTO wp_weep_terms (SELECT 0, size, LOWER(size), 0, 0 FROM products);

INSERT IGNORE INTO wp_weep_terms (SELECT 0, `group`, LOWER(`group`), 0, 0 FROM products);

#Setup term taxonomy

INSERT INTO `wp_weep_term_taxonomy` (term_taxonomy_id, term_id, taxonomy)
(
SELECT 0, terms.term_id, 'pa_region'
FROM wp_weep_terms AS terms
INNER JOIN products AS p ON p.region = terms.name
);

INSERT IGNORE INTO `wp_weep_term_taxonomy` (term_taxonomy_id, term_id, taxonomy)
(
SELECT 0, terms.term_id, 'pa_size'
FROM wp_weep_terms AS terms
INNER JOIN products AS p ON p.size = terms.name
);

INSERT IGNORE INTO `wp_weep_term_taxonomy` (term_taxonomy_id, term_id, taxonomy)
(
SELECT 0, terms.term_id, 'pa_group'
FROM wp_weep_terms AS terms
INNER JOIN products AS p ON p.`group` = terms.name
);

#Setup term taxonoy relationship to page
INSERT INTO wp_weep_term_relationships
(
SELECT p.page_id, tax.term_taxonomy_id, 0
FROM products AS p
JOIN wp_weep_terms AS t ON t.name = p.region
JOIN wp_weep_term_taxonomy AS tax ON tax.term_id = t.term_id
);

INSERT INTO wp_weep_term_relationships
(
SELECT p.page_id, tax.term_taxonomy_id, 0
FROM products AS p
JOIN wp_weep_terms AS t ON t.name = p.size
JOIN wp_weep_term_taxonomy AS tax ON tax.term_id = t.term_id
);

INSERT INTO wp_weep_term_relationships
(
SELECT p.page_id, tax.term_taxonomy_id, 0
FROM products AS p
JOIN wp_weep_terms AS t ON t.name = p.`group`
JOIN wp_weep_term_taxonomy AS tax ON tax.term_id = t.term_id
);

#Add product meta so params display
INSERT INTO wp_weep_postmeta
(SELECT 0, page_id, '_product_attributes', 'a:3:{s:8:"pa_group";a:6:{s:4:"name";s:8:"pa_group";s:5:"value";s:0:"";s:8:"position";s:1:"0";s:10:"is_visible";i:1;s:12:"is_variation";i:0;s:11:"is_taxonomy";i:1;}s:7:"pa_size";a:6:{s:4:"name";s:7:"pa_size";s:5:"value";s:0:"";s:8:"position";s:1:"0";s:10:"is_visible";i:1;s:12:"is_variation";i:0;s:11:"is_taxonomy";i:1;}s:9:"pa_region";a:6:{s:4:"name";s:9:"pa_region";s:5:"value";s:0:"";s:8:"position";s:1:"0";s:10:"is_visible";i:1;s:12:"is_variation";i:0;s:11:"is_taxonomy";i:1;}}' FROM products WHERE page_id NOT IN (SELECT post_id FROM wp_weep_postmeta WHERE meta_key = '_product_attributes'));

INSERT INTO wp_weep_postmeta (SELECT 0, page_id, '_visibility', 'visible' FROM products);

 *
 */