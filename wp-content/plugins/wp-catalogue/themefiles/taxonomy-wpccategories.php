<?php get_header(); ?>

<div class="page-content fullwidth-temp">
    <div class="container post-wrap">
        <div class="row-fluid">
            <div id="content" class="span12">
                <div class="post" id="post-<?php the_ID(); ?>">
                    <div class="skepost">
<!--Content-->

<?php echo get_option('inn_temp_head'); ?>	 
	<?php echo do_shortcode('[wp-catalogue]'); ?>
<div class="clear"></div>
<?php echo get_option('inn_temp_foot'); ?>	

<!--/Content-->
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<?php get_footer(); ?>
